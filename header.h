

.macro HEX_NIBBLE reg
	LOCAL letter,end
	cmp $10,\reg
	jae letter
	add $0, \reg
	jmp end

letter:
	add $0x37,\reg
end:
.endm

.macro BEGIN
	.code16
	cli
	ljmp $0, $1f
1:
	xor %ax, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs
	mov %ax, %bp
	mov %ax, %ss
	mov %bp, %sp

.endm
