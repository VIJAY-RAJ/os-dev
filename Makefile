
GAS_EXT:=.S
GAS:= gcc
OBJ:= $(patsubst %.S,%.o,$(wildcard *.S))	#$(patsubst PATTERN,REPLACEMENT,TEXT)
OUT_FILE:=boot.img
HEADER:=header.h
LD_SCRIPT:=linker.ld

%.o: %.S $(HEADER)
	$(GAS) -c -o $@ $<

$(OUT_FILE): $(OBJ)
	ld --oformat=binary -T '$(LD_SCRIPT)' $< -o $@
